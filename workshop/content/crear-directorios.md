#   Crear y cambiar directorios

En este paso, utilizará los comandos cambiar directorio **(cd)**, crear directorio **(mkdir)** y enumerar directorio **(ls)**.

1. Escriba **pwd**, verá el directorio acutla
2. Cree un directorio
```bash
mkdir directorio1
```
3. Liste directorios
```bash
ls
```
4. Cambie de directorio
```bash
cd directorio1
```