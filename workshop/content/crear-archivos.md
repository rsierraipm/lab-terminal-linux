#  Crear archivos de texto

1. Escriba **echo This is doc1.txt > doc1.txt**
2. Liste los archivos
```bash
ls
```
4. Ver el contenido del fichero
```bash
cat doc1.txt
```