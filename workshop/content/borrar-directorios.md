#  Eliminar directorios

1. Cree el directorio **dirborrar**
```bash
mkdir dirborrar
```
2. Liste los archivos y directorios
```bash
ls
```
3. borrar el directorio **dirborrar**
```bash
rmdir dirborrar
```
4. Liste los archivos y directorios
```bash
ls
```