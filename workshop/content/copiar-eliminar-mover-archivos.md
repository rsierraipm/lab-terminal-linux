#  Copiar, eliminar y mover archivos

1. Copiar archivo
```bash
cp doc1.txt doc2.txt
```
2. Liste los archivos
```bash
ls
```
3. Mover archivo
```bash
mv doc2.txt doc3.txt
```
4. Liste los archivos
```bash
ls
```
5. Eliminar archivo
```bash
rm doc3.txt
```
6. Liste los archivos
```bash
ls
```